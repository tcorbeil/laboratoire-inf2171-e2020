nouvEtu: LDA     0,i
         LDX     0,i
         STA     chiffre,d
         STA     nombre,d
         STA     cptTabN,d
         STA     totalDiv,d 
         STA     plus1,d
         STA     moyenne,d
         STA     noteFin,d 
         STA     moyenneE,d
         STA     sommeEct,d
         STA     ecartTp,d
         STA     note1,d
         STA     note2,d
         STA     note3,d
         STA     totalNF,d
         STA     cptSmEct,d

         STRO    msgEnt,d
         LDA     cptEtu,d
         ASRA    
         ADDA    1,i
         STA     nbEtu,d
         DECO    nbEtu,d
         CPA     1,i
         BRNE    sautAutr
         STRO    msgPrem,d 
         BR      bclLect
sautAutr:STRO    msgAutre,d  

bclLect: CHARI   caract,d 
         LDA     avCaract,d
         CPX     '3',i
         BRGE    err
         CPA     ',',i
         BREQ    ajoutNb
         CPA     '\n',i
         BREQ    finLect
         CPA     '0',i
         BRLT    err
         CPA     '9',i
         BRGT    err
         SUBA    '0',i
         STA     chiffre,d
         LDA     nombre,d
         ASLA
         ASLA
         ADDA    nombre,d
         ASLA
         ADDA    chiffre,d
         STA     nombre,d
         ADDX    1,i
         BR      bclLect

ajoutNb: LDA     nombre,d
         CPA     100,i
         BRGT    err
         LDX     cptTabN,d
         CPX     4,i
         BRGT    err
         STA     tabNote,x
         ADDX    2,i
         STX     cptTabN,d 
         LDX     0,i
         STX     nombre,d
         BR      bclLect

finLect: CPX     0,i
         BRNE    sautFin
         LDX     cptTabN,d
         CPX     0,i
         BREQ    calulGr
         BR      err
sautFin: LDA     nombre,d
         CPA     100,i
         BRGT    err
         LDX     cptTabN,d
         CPX     6,i
         BRLT    err
         STA     tabNote,x
         ADDX    2,i
         STX     cptTabN,d 
         BR      calcEtu

;MODIF AVEC BOUCLE
;CALCUL MOYENNE
calcEtu: LDX     0,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d 
         LDX     2,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d
         LDX     4,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d
         LDX     6,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d 

         LDA     totalDiv,d
         STA     divid,d
         LDA     4,i
         STA     divis,d
         CALL    div
         LDA     res,d
         LDX     reste,d
         CPX     2,i
         BRLT    sautP1
         ADDA    1,i
sautP1:  STA     moyenne,d
;CALCUL  NOTE FINALE
         CALL    pgTab
         LDA     plusGr,d
         STA     note1,d
         CALL    pgTab
         LDA     plusGr,d
         STA     note2,d
         CALL    pgTab
         LDA     plusGr,d
         STA     note3,d
         CALL    normTab
         
         LDA     note1,d
         STA     multiPc,d 
         LDA     45,i
         STA     multi,d 
         CALL    mult
         LDA     resM,d
         ADDA    totalNF,d
         STA     totalNF,d

         LDA     note2,d
         STA     multiPc,d
         LDA     35,i
         STA     multi,d
         CALL    mult
         LDA     resM,d
         ADDA    totalNF,d
         STA     totalNF,d

         LDA     note3,d
         STA     multiPc,d
         LDA     20,i
         STA     multi,d
         CALL    mult
         LDA     resM,d
         ADDA    totalNF,d
         STA     totalNF,d

         LDA     totalNF,d
         STA     divid,d
         LDA     10,i
         STA     divis,d
         ;v�rifier arrondi
         CALL    div
         LDA     res,d
         STA     noteFin,d
         
;CALCUL �CART TYPE
         LDA     noteFin,d
         STA     divid,d
         LDA     10,i
         STA     divis,d 
         CALL    div
         LDA     res,d
         LDX     reste,d
         CPX     5,i
         BRLT    sautANF
         ADDA    1,i
sautANF: STA     moyenneE,d

         LDX     0,i
         STX     cptSmEct,d
bclSomm: LDX     cptSmEct,d
         CPX     6,i 
         BRGT    finBclSm
         LDA     tabNote,x
         SUBA    moyenneE,d
         CPA     0,i
         BRGE    sautNeg
         NEGA    
sautNeg: STA     multiPc,d
         STA     multi,d
         CALL    mult
         LDA     resM,d
         ADDA    sommeEct,d
         STA     sommeEct,d
         LDX     cptSmEct,d
         ADDX    2,i
         STX     cptSmEct,d
         BR      bclSomm

finBclSm:LDA     sommeEct,d
         STA     divid,d
         LDA     4,i
         STA     divis,d
         CALL    div
         LDA     res,d
         CALL    RACINE
         STA     ecartTp,d
;AFFICHAGE ETUDIANT
         STRO    msgNF,d
         LDA     noteFin,d
         STA     divid,d
         LDA     10,i
         STA     divis,d
         CALL    div
         DECO    res,d
         CHARO   '.',i
         DECO    reste,d
         CHARO   '%',i

         STRO    msgMoy,d
         LDA     moyenne,d
         STA     divid,d
         LDA     10,i
         STA     divis,d
         CALL    div
         DECO    res,d
         CHARO   '.',i
         DECO    reste,d
         CHARO   '%',i

         STRO    msgEc,d
         DECO    ecartTp,d
         CHARO   '\n',i

         LDX     cptEtu,d
         LDA     noteFin,d
         STA     tabNoteF,x
         ADDX    2,i
         STX     cptEtu,d

         BR      nouvEtu

calulGr: LDX     0,i 
bclSMNF: CPX     cptEtu,d
         BRGE    finSMNF
         LDA     tabNoteF,x
         ADDA    sommeNFG,d
         STA     sommeNFG,D
         ADDX    2,i
         BR      bclSMNF

finSMNF: LDA     sommeNFG,d
         STA     divid,d
         LDA     cptEtu,d
         ASRA
         STA     divis,d
         CALL    div
         LDA     res,d
         STA     moyGr,d
         DECO    moyGr,d

;CACUL ECART TYPE NF
         LDA     moyGr,d
         STA     divid,d
         LDA     10,i
         STA     divis,d 
         CALL    div
         LDA     res,d
         LDX     reste,d
         CPX     5,i
         BRLT    sautAMG
         ADDA    1,i
sautAMG: STA     moyGrEct,d

         LDX     0,i
         STX     cptSmGr,d
bclSommG:LDX     cptSmGr,d
         CPX     cptEtu,d 
         BRGE    finBclSG
         LDA     tabNoteF,x
         STA     divid,d
         LDA     10,i
         STA     divis,d
         CALL    div
         LDA     res,d
         LDX     rest,d      
         CPX     5,i
         BRLT    sautArGr
         ADDA    1,i
sautArGr:SUBA    moyGrEct,d
         CPA     0,i
         BRGE    sautNegG
         NEGA    
sautNegG:STA     multiPc,d
         STA     multi,d
         CALL    mult
         LDA     resM,d
         ADDA    sommeGRE,d
         STA     sommeGRE,d
         LDX     cptSmEct,d
         ADDX    2,i
         STX     cptSmGr,d
         BR      bclSomm

finBclSG:NOP ;ERROR: Operand specifier expected after mnemonic.

         BR      fin

err:     STRO    msgErr,d 
fin:     STOP

;#######################
;Sous programme div
;#######################
div:     LDA     divid,d
         LDX     0,i
bclDiv:  CPA     0,i
         BRLT    finDiv
         SUBA    divis,d 
         ADDX    1,i
         BR      bclDiv
finDiv:  ADDA    divis,d 
         SUBX    1,i
         STA     reste,d
         STX     res,d
         RET0

;#######################
;Sous programme mult
;#######################
mult:    LDA     0,i
         LDX     multi,d
bclMult: CPX     0,i
         BRLT    finMult
         ADDA    multiPc,d 
         SUBX    1,i
         BR      bclMult
finMult: SUBA    multiPc,d 
         ADDX    1,i
         STA     resM,d
         RET0

;#######################
;Sous programme Racine
;#######################
;
;RACINE: effectue la racine carr�e
;
; Passage des param�tres par le registre A
;
; IN: reg A : nombre � extraire
;     reg X : quelconque
;
; OUT: reg A : racine carr�e
;      reg X : 0
;
RACINE:  STA     nombreR,d   ; nombre � extraire
         LDA     0,i         ; initialise la racine carree
         STA     racine,d    
         CPA     nombreR,d   
         BRGE    FINRAC      
suivanT: LDA     racine,d    
         ADDA    1,i         
         STA     racine,d    
         LDX     racine,d    ; initialise le multiplicateur
         LDA     0,i         ; initialise le produit
multI:   ADDA    racine,d    
         SUBX    1,i         
         BRNE    multI       ; fin de la multiplication lorsque le nombre d'occurrences = 0
         CPA     nombreR,d   
         BREQ    FINRAC      ; fin du calcul lorsque produit = nombre
         BRLT    suivanT     ; calcul suivant lorsque produit < nombre
         SUBA    nombreR,d   
         CPA     racine,d    
         BRLT    FINRAC      ; fin lorsque produit - nombre < racine carree
         LDA     racine,d    ; arrondir lorsque produit - nombre >= racine carree
         SUBA    1,i         
         STA     racine,d    
FINRAC:  LDA     racine,d    
         RET0

;#######################
;Sous programme pgTab
;#######################
pgTab:   LDX     0,i
         LDA     0,i
         STA     plusGr,d
bclPg:   LDA     tabNote,x
         CPA     plusGr,d
         BRLE    sautPg
         STA     plusGr,d
         STX     indPlusG,d
sautPg:  ADDX    2,i
         CPX     6,i
         BRLE    bclPg
         LDX     indPlusG,d 
         LDA     tabNote,x
         NEGA
         STA     tabNote,x
         RET0

;#######################
;Sous programme normTab
;#######################
normTab: LDX     0,i
         LDA     0,i 
bclNorm: LDA     tabNote,x
         CPA     0,i
         BRGE    sautNorm 
         NEGA    
         STA     tabNote,x
sautNorm:ADDX    2,i
         CPX     6,i
         BRLE    bclNorm
         RET0


msgEnt:  .ASCII  "Entrez les notes du \x00"
msgPrem: .ASCII  "er �tudiant: \x00"
msgAutre:.ASCII  "i�me �tudiant: \x00"
msgErr:  .ASCII  "Nombre entr� invalide!\x00"
msgNF:   .ASCII  "Note Finale: \x00"
msgMoy:  .ASCII  "  Moyenne: \x00"
msgEc:   .ASCII  "  Ecart-type: \x00"
avCaract:.BLOCK  1
caract:  .BLOCK  1
chiffre: .BLOCK  2           ;#2d
nombre:  .BLOCK  2           ;#2d
tabNote: .BLOCK  8 
cptTabN: .BLOCK  2           ;#2d
totalDiv:.BLOCK  2           ;#2d 
plus1:   .BLOCK  2           ;#2d
moyenne: .BLOCK  2           ;#2d
noteFin: .BLOCK  2           ;#2d 
moyenneE:.BLOCK  2           ;#2d
sommeEct:.BLOCK  2           ;#2d
ecartTp: .BLOCK  2           ;#2d
note1:   .BLOCK  2           ;#2d
note2:   .BLOCK  2           ;#2d
note3:   .BLOCK  2           ;#2d
totalNF: .BLOCK  2           ;#2d
cptSmEct:.BLOCK  2           ;#2d
nbEtu:   .BLOCK  2

tabNoteF:.BLOCK  50         
cptEtu:  .BLOCK  2           ;#2d
sommeNFG:.BLOCK  2           ;#2d
moyGr:   .BLOCK  2           ;#2d
moyGrEct:.BLOCK  2           ;#2d
cptSmGr: .BLOCK  2           ;#2d
sommeGRE:.BLOCK  2
                

;#######################
;Variable sous programme div
;#######################

divid:   .BLOCK  2
divis:   .BLOCK  2
res:     .BLOCK  2
reste:   .BLOCK  2

;#######################
;Variable sous programme mult
;#######################

multiPc: .BLOCK  2
multi:   .BLOCK  2
resM:     .BLOCK  2 

;##############################
;Variable sous programme Racine
;##############################

nombreR: .BLOCK  2           ; #2d    nombre
racine:  .BLOCK  2           ; #2d    racine carr�e

;#######################
;Variable sous programme pgTab
;#######################

plusGr:  .BLOCK 2
indPlusG:.BLOCK 2
.END








