         LDA     0,i
         LDX     0,i
bclLect: CHARI   caract,d 
         LDA     avCaract,d
         CPX     '3',i
         BRGE    err
         CPA     ',',i
         BREQ    ajoutNb
         CPA     '\n',i
         BREQ    finLect
         CPA     '0',i
         BRLT    err
         CPA     '9',i
         BRGT    err
         SUBA    '0',i
         STA     chiffre,d
         LDA     nombre,d
         ASLA
         ASLA
         ADDA    nombre,d
         ASLA
         ADDA    chiffre,d
         STA     nombre,d
         ADDX    1,i
         BR      bclLect

ajoutNb: LDA     nombre,d
         CPA     100,i
         BRGT    err
         LDX     cptTabN,d
         CPX     4,i
         BRGT    err
         STA     tabNote,x
         ADDX    2,i
         STX     cptTabN,d 
         LDX     0,i
         STX     nombre,d
         BR      bclLect

finLect: CPX     0,i
         BRNE    sautFin
         LDX     cptTabN,d
         CPX     0,i
         BREQ    fin
         BR      err
sautFin: LDA     nombre,d
         CPA     100,i
         BRGT    err
         LDX     cptTabN,d
         CPX     6,i
         BRLT    err
         STA     tabNote,x
         ADDX    2,i
         STX     cptTabN,d 
         BR      calcEtu

;MODIF AVEC BOUCLE
calcEtu: LDX     0,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d 
         LDX     2,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d
         LDX     4,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d
         LDX     6,i
         LDA     tabNote,x
         ASLA    
         ASLA    
         ADDA    tabNote,x
         ASLA  
         ADDA    totalDiv,d
         STA     totalDiv,d 

         LDA     totalDiv,d
         STA     divid,d
         LDA     4,i
         STA     divis,d
         CALL    div
         LDA     res,d
         LDX     reste,d
         CPX     2,i
         BRLT    sautP1
         ADDA    1,i
sautP1:  STA     moyenne,d

         CALL    pgTab
         LDA     plusGr,d
         STA     note1,d
         CALL    pgTab
         LDA     plusGr,d
         STA     note2,d
         CALL    pgTab
         LDA     plusGr,d
         STA     note3,d
         CALL    normTab
         
         LDA     note1,d
         STA     multiPc,d 
         LDA     45,i
         STA     multi,d 
         CALL    mult
         LDA     resM,d
         ADDA    totalNF,d
         STA     totalNF,d

         LDA     note2,d
         STA     multiPc,d
         LDA     35,i
         STA     multi,d
         CALL    mult
         LDA     resM,d
         ADDA    totalNF,d
         STA     totalNF,d

         LDA     note3,d
         STA     multiPc,d
         LDA     20,i
         STA     multi,d
         CALL    mult
         LDA     resM,d
         ADDA    totalNF,d
         STA     totalNF,d

         DECO    totalNF,d
         BR      fin

err:     STRO    msgErr,d 
fin:     STOP

;#######################
;Sous programme div
;#######################
div:     LDA     divid,d
         LDX     0,i
bclDiv:  CPA     0,i
         BRLT    finDiv
         SUBA    divis,d 
         ADDX    1,i
         BR      bclDiv
finDiv:  ADDA    divis,d 
         SUBX    1,i
         STA     reste,d
         STX     res,d
         RET0

;#######################
;Sous programme mult
;#######################
mult:    LDA     0,i
         LDX     multi,d
bclMult: CPA     0,i
         BRLT    finMult
         ADDA    multiPc,d 
         SUBX    1,i
         BR      bclMult
finMult: SUBA    multiPc,d 
         ADDX    1,i
         STA     resM,d
         RET0

;#######################
;Sous programme pgTab
;#######################
pgTab:   LDX     0,i
         LDA     0,i
         STA     plusGr,d
bclPg:   LDA     tabNote,x
         CPA     plusGr,d
         BRLE    sautPg
         STA     plusGr,d
         STX     indPlusG,d
sautPg:  ADDX    2,i
         CPX     6,i
         BRLE    bclPg
         LDX     indPlusG,d 
         LDA     tabNote,x
         NEGA
         STA     tabNote,x
         RET0

;#######################
;Sous programme normTab
;#######################
normTab: LDX     0,i
         LDA     0,i 
bclNorm: LDA     tabNote,x
         CPA     0,i
         BRGE    sautNorm 
         NEGA    
         STA     tabNote,x
sautNorm:ADDX    2,i
         CPX     6,i
         BRLE    bclNorm
         RET0


msgErr:  .ASCII  "Nombre entr� invalide!\x00"
avCaract:.BLOCK  1
caract:  .BLOCK  1
chiffre: .BLOCK  2           ;#2d
nombre:  .BLOCK  2           ;#2d
tabNote: .BLOCK  8 
cptTabN: .BLOCK  2           ;#2d
totalDiv:.BLOCK  2           ;#2d 
plus1:   .BLOCK  2           ;#2d
moyenne: .BLOCK  2 
note1:   .BLOCK  2           ;#2d
note2:   .BLOCK  2           ;#2d
note3:   .BLOCK  2           ;#2d
totalNF: .BLOCK  2           ;#2d
                

;#######################
;Variable sous programme div
;#######################

divid:   .BLOCK  2
divis:   .BLOCK  2
res:     .BLOCK  2
reste:   .BLOCK  2

;#######################
;Variable sous programme mult
;#######################

multiPc: .BLOCK  2
multi:   .BLOCK  2
resM:     .BLOCK  2 


;#######################
;Variable sous programme pgTab
;#######################

plusGr:  .BLOCK 2
indPlusG:.BLOCK 2
.END








