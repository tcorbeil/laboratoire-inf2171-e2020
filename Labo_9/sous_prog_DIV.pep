         LDA     0,i
         LDX     0,i
         SUBSP   8,i         ;#reste #res #divis #divid 
         LDA     25,i
         STA     0,s
         LDA     4,i
         STA     2,s
bcl:     CPX     4,i
         BRGE    finBcl
         CALL    div
         DECO    4,s
         ADDX    1,i
         BR      bcl

finBcl:  ADDSP   8,i        ;#reste #res #divis #divid  
         STOP
;###############################
SauvA:  .EQUATE 0 ;#2d
SauvX:  .EQUATE 2 ;#2d
;#---------------------
divid:  .EQUATE 6 ;#2d
divis:  .EQUATE 8 ;#2d
res:    .EQUATE 10 ;#2d
reste:  .EQUATE 12 ;#2d
;###############################
div:     SUBSP   4,i         ;#SauvX #SauvA
         STA     SauvA,s
         STX     SauvX,s
         LDA     divid,s 
         LDX     0,i
bclDiv:  CPA     0,i
         BRLT    finDiv
         SUBA    divis,s
         ADDX    1,i
         BR      bclDiv
finDiv:  SUBX    1,i
         ADDA    divis,s 
         STA     reste,s
         STX     res,s 
         LDA     SauvA,s
         LDX     SauvX,s
         RET4    ;#SauvX #SauvA
 
         .END