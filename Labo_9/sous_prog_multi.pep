         LDA     0,i
         LDX     0,i
         SUBSP   6,i         ;#res #mucande #multip
         LDA     10,i
         STA     0,s
         LDA     4,i
         STA     2,s
bcl:     CPX     4,i
         BRGE   finBcl
         CALL    multi
         DECO    4,s
         ADDX    1,i
         BR      bcl

finBcl:  ADDSP   6,i        ;#res #mucande #multip  
         STOP
;###############################
SauvA:   .EQUATE 0 ;#2d
SauvX:   .EQUATE 2 ;#2d
multip:  .EQUATE 6 ;#2d
mucande: .EQUATE 8 ;#2d
res:     .EQUATE 10 ;#2d
;###############################
multi:   SUBSP   4,i         ;#SauvX #SauvA
         STA     SauvA,s
         STX     SauvX,s
         LDA     0,i 
         LDX     multip,s
bclMulti:CPX     0,i
         BRLT    finMulti
         ADDA    mucande,s
         SUBX    1,i
         BR      bclMulti
finMulti:ADDX    1,i
         SUBA    mucande,s 
         STA     res,s
         LDA     SauvA,s
         LDX     SauvX,s
         RET4    ;#SauvX #SauvA
 
         .END