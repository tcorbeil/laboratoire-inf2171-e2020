; ************************************************************
;       Programme: PREMIER1.TXT     version PEP813 sous Windows
;
;       version 1: la version de base
;
;       Mon premier programme qui affiche la somme de 2 nombres sign�s.
;
;       auteur:         Bernard Martin
;       code permanent: non applicable
;       courriel:       martin.bernard@uqam.ca
;       date:           Et� 2020
;       cours:          INF2171
; ***********************************************************
;
         LDA     0,i
bclLct:  DECI    nbLu,d   ; lecture du premier nombre
         LDX     nbLu,d
         CPX     0,i
         BREQ    AFF
         ASRX    
         BRC     mult8
         ASRX    
         BRC     mult8
         ASRX    
         BRC     mult8
         ADDA    nbLu,d
         BR      bclLct

mult8:   STRO    msgErr,d
         BR      fin

AFF:     STA     total,d
         DECO    total,d
         
fin:     STOP                
;
msgErr:  .ASCII  "Ce nombre n'est pas un multiple de 8!\x00"
nbLu:    .BLOCK  2           ; #2d valeur initiale z�ro (2 octets qui seront affich�s en d�cimal)
total:   .BLOCK  2           ; #2d valeur initiale z�ro
;
         .END                  