debut:   LDA     0,i
         LDX     0,i
         LDA     avCaract,d
         CPA     0,i
         BRNE    sautZero
bclEsp:  CHARI   caract,d
         LDA     avCaract,d
sautZero:CPA     ' ',i
         BREQ    bclEsp
         BR      sautEnt          

bclLect: CPX     1,i
         BRGT    lectTir
         CHARI   caract,d
sautEnt: LDA     avCaract,d
         CPA     'A',i
         BRLT    err
         CPA     'Z',i
         BRGT    err
         STBYTEA chneTmp,x
         ADDX    1,i
         BR      bclLect

lectTir: CHARI   caract,d
         LDA     avCaract,d
         CPA     '-',i
         BRNE    err
         STBYTEA chneTmp,x
         ADDX    1,i

bclLect2:CPX     7,i
         BRGT    err
         CHARI   caract,d
         LDA     avCaract,d
         CPX     3,i
         BREQ    sautComp
         CPA     '\n',i
         BREQ    ajoutLsF
         CPA     ' ',i
         BREQ    ajoutLst
sautComp:CPA     '0',i
         BRLT    err
         CPA     '9',i
         BRLE    sautMin
         CPA     'A',i
         BRLT    err
         CPA     'Z',i
         BRLE    sautMin
         CPA     'a',i
         BRLT    err
         CPA     'z',i
         BRGT    err
         SUBA    'a',i
         ADDA    'A',i
sautMin: STBYTEA chneTmp,x
         ADDX    1,i
         BR      bclLect2

ajoutLsF:LDA     1,i
         STA     ajoutFin,d
ajoutLst:LDA     0,i
         STBYTEA chneTmp,x 

         LDA     blocAct,d
         STA     blocPrec,d    

         LDA     10,i
         CALL    new
         STX     blocAct,d         	

         LDX     0,i
         LDA     0,i
bclTrsf: LDBYTEA chneTmp,x
         CPA     '\x00',i
         BREQ    finTrsf
         ADDX    blocAct,d
;        STBYTEA 0,x
         STX     addCar,d
         STBYTEA addCar,n
         SUBX    blocAct,d
         ADDX    1,i
         BR      bclTrsf

finTrsf: LDA     0,i
         ADDX    blocAct,d
         STX     addCar,d
         STBYTEA addCar,n
         SUBX    blocAct,d
         LDX     blocAct,d
         ADDX    next,i
         STX     addCar,d
         LDA     0,i
         STA     addCar,n

         LDX     blocPrec,d
         CPX     0,i
         BREQ    sautPr
         ADDX    next,i
         STX     addCar,d
         LDA     blocAct,d
         STA     addCar,n
sautPr:  LDA     ajoutFin,d
         CPA     1,i
         BREQ    fin  
         BR      debut

err:     STRO    msgErr,d
         BR      fin   

fin:     LDX     heap,i
         CPX     hpPtr,d
         BREQ    arret
         SUBSP   10,i ;#sauvX, #sauvA, #addPt, #chaine2, #chaine1 
         STX     0,s
         STX     blocIter,d
         LDX     next,x
         CPA     0,i
         BREQ    affUni
         STX     2,s 
         CALL    compChne
         LDA     4,s
         STA     addrPt,d
         BR      bclPt

nouvPt:  LDA     heap,i
         STA     blocIter,d
bclPt:   LDA     addrPt,d
         STA     0,s
         LDA     blocIter,d
         STA     2,s
         CALL    compChne
         LDA     4,s
         STA     addrPt,d
         LDX     blocIter,d
         LDX     next,x
         STX     blocIter,d
         CPX     0,i
         BREQ    aff1pt
         BR      bclPt

aff1pt:  LDA     0,i
         LDBYTEA addrPt,n
         CPA     '{',i
         BREQ    arret
         STRO    addrPt,n
         CHARO   ' ',i
         LDX     addrPt,d
         LDA     0,i
         LDBYTEA '{',i
         STBYTEA 0,x
         LDBYTEA '{',i
         STBYTEA 1,x         
         LDBYTEA '{',i
         STBYTEA 2,x         
         LDBYTEA '{',i
         STBYTEA 3,x
         LDBYTEA '{',i
         STBYTEA 4,x
         LDBYTEA '{',i
         STBYTEA 5,x
         LDBYTEA '{',i
         STBYTEA 6,x
         LDBYTEA '\x00',i
         STBYTEA 7,x
         BR      nouvPt 

affUni:  STRO    blocIter,n
         ADDSP   10,i ;#sauvX, #sauvA, #addPt, #chaine2, #chaine1    

arret:   ADDSP   10,i ;#sauvX, #sauvA, #addPt, #chaine2, #chaine1 
         STOP

avCaract:.BLOCK  1
caract:  .BLOCK  1
chneTmp: .BLOCK  8
msgErr:  .ASCII  "Entree Invalide!\x00"
addCar:  .BLOCK  2;#2h
ajoutFin:.BLOCK  2;#2d

blocAct: .BLOCK  2;#2h
blocPrec:.BLOCK  2;#2h

blocIter:.BLOCK  2;#2h
addrPt:  .BLOCK  2;#2h 
         
;####################
car2Cvn:.EQUATE 0           ;#2h
;--------------------------------
chaine1: .EQUATE 4           ;#2h
chaine2: .EQUATE 6           ;#2h
addPt:   .EQUATE 8           ;#2h
sauvA:   .EQUATE 10          ;#2d
sauvX:   .EQUATE 12          ;#2d


;###################
compChne:STA     sauvA,s
         STX     sauvX,s 
         LDX     0,i
         LDA     0,i
         SUBSP   2,i         ;#car2Cvn 
         STA     car2Cvn,s
bclComp: LDBYTEA chaine2,sxf
         STA     car2Cvn,s 
         LDBYTEA chaine1,sxf
         CPA     car2Cvn,s 
         BREQ    sautCpCh
         BRGT    chaine2P
         BRLT    chaine1P
sautCpCh:CPA     0,i 
         BREQ    chaine1P
         ADDX    1,i
         BR      bclComp

chaine1P:LDA     chaine1,s
         STA     addPt,s
         BR      finComp

chaine2P:LDA     chaine2,s
         STA     addPt,s
         BR      finComp

finComp: LDA     sauvA,s
         LDX     sauvX,s
         RET2    ;#car2Cvn 

;####################
chaineBk:.EQUATE 0
next:    .EQUATE 8
;####################

;******* operator new
;        Precondition: A contains number of bytes
;        Postcondition: X contains pointer to bytes
new:     LDX     hpPtr,d     ;returned pointer
         ADDA    hpPtr,d     ;allocate from heap
         STA     hpPtr,d     ;update hpPtr
         RET0    
            
hpPtr:   .ADDRSS heap           ;#2h ADDRSS heap (address of next free byte)
heap:    .BLOCK  1           ;first byte in the heap
         .END 