﻿# Laboratoire-INF2171-E2020

Solution des laboratoires pour le cours INF2171 à la session d'été 2020

##Solutions des laboratoires

1. Semaine 1: [Exercices sur les nombres](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_1)
2. Semaine 2: [Exercices sur les nombres(suite)](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_2)
3. Semaine 3: [Intro à PEP8](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_3)
4. Semaine 4: [TP1 première partie](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_4)
5. Semaine 5: [TP1 seconde partie](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_5)
6. Semaine 6: [Tableaux](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_6)
7. Semaine 7: [TP2 première partie](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_7)
8. Semaine 8: [TP2 deuxième partie](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_8)
9. Semaine 9: [Sous programmes](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_9)
10. Semaine 10: [TP3 première partie](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_10)
11. Semaine 11: [TP3 deuxième partie](https://gitlab.com/tcorbeil/laboratoire-inf2171-e2020/-/tree/master/Labo_11)